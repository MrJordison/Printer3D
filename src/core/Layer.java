package core;

import fill_polygon.Polygon2i;
import utils.ImageUtils;
import utils.Math.Vector2i;
import utils.Math.Vector3f;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Layer {

    PrintableModel model;
    List<Vector3f> vectorial_pts;
    List<Vector2i> discret_pts;

    /**
     * Constructeur de la classe Layer, prend en paramètre une liste de points vectoriels et le modèle auquel appartient la couche
     * @param model
     * @param pts
     */
    public Layer(PrintableModel model, List<Vector3f> pts){
        this.vectorial_pts = pts;
        this.model = model;
    }

    /**
     * Fonction qui va dessiner le contour brut d'une couche
     * @param pixels_per_unit
     * @return
     */
    public BufferedImage draw_brut_raster(float pixels_per_unit){
        //Cr�ation d'une image vide, avec les dimensions pass�es en param�tres
        BufferedImage raster = new BufferedImage(model.layers_dimensions[0], model.layers_dimensions[1], BufferedImage.TYPE_INT_RGB);
        Graphics2D writer = raster.createGraphics();
        writer.setColor(new Color(255, 255, 255));


	if(discret_pts == null)
	    discretise_layer(pixels_per_unit);

        int deb,end;
        //Tracage des segments obtenus dans l'image
        for (deb = 0; deb < discret_pts.size(); deb+=2) {
            if (deb == discret_pts.size() - 1)
                end = 0;
            else
                end = deb + 1;

            writer.drawLine(
                    discret_pts.get(deb).m_X,
                    discret_pts.get(deb).m_Y,
                    discret_pts.get(end).m_X,
                    discret_pts.get(end).m_Y
            );
        }

        writer.dispose();

        return raster;
    }


    /**
     * Fonction qui va dessiner le contour brut et le remplir
     * @param pixels_per_unit
     * @return
     */
    public BufferedImage filled_raster(float pixels_per_unit){

        if(discret_pts == null)
            discretise_layer(pixels_per_unit);
        Polygon2i p = new Polygon2i(discret_pts, true);
        BufferedImage raster = Polygon2i.fill_polygon(p, model.layers_dimensions);

        return raster;
    }

    /**
     * Fonction qui dessine le chemin discrétisé suivi par la buse ainsi que la coque externe de la couche (coque externe qui est le contour brut)
     * @param pixels_per_unit
     * @param nozzle_diameter
     * @return
     */
    public BufferedImage shell_path_raster(float pixels_per_unit, float nozzle_diameter){
        int nb_erosions = (int) ((nozzle_diameter / 2f) * pixels_per_unit);

        BufferedImage filled = filled_raster(pixels_per_unit), brut = draw_brut_raster(pixels_per_unit);

        BufferedImage result = ImageUtils.cloneBufferedImage(filled);

        //Erosion jusqu'à avoir enlevé l'épaisseur souhaitée
        for(int i = 0; i < nb_erosions; ++i)
            result = ImageUtils.erosion(result);

        //Récupération du contour intérieur avec une différence entre le result et une version érodée du result
        result = ImageUtils.difference(result, ImageUtils.erosion(result));

        return result;
    }

    /**
     * Fonction qui va récuperer tous les chemins de contours dans le raster représentant le chemin de la buse, retourne une liste de contours, chacun etant une liste de déplacements
     * si tolerance_smoothing est supérieur à 0, un lissage par d&q est appliqué sur les contours obtenus
     * @param pixels_per_unit
     * @param nozzle_diameter
     * @param tolerance_smoothing la tolérance de lissage
     * @return
     */
    private List<List<Vector2i>> get_contours(float pixels_per_unit, float nozzle_diameter, float tolerance_smoothing){
        BufferedImage shell_path = shell_path_raster(pixels_per_unit, nozzle_diameter);

        List<List<Vector2i>> contours = new ArrayList<>();

        Vector2i[] neighbours = new Vector2i[]{
                new Vector2i(-1,0),
                new Vector2i(0,-1),
                new Vector2i(1,0),
                new Vector2i(0,1),
                new Vector2i(-1,-1),
                new Vector2i(1,-1),
                new Vector2i(1,1),
                new Vector2i(-1,1)
        };


        //Variables buffer pour le parcours de l'image
        List<Vector2i> contour_buffer;
        Vector2i depart, position, neighbour;
        boolean adjacent_depart, end, contains;

        //Parcours de tous les pixels de l'image pour trouver tous les contours
        for(int x = 0; x < model.layers_dimensions[0]; ++x){
            for(int y = 0; y < model.layers_dimensions[1]; ++y) {

                //Si le pixel n'est pas noir
                if (Color.black.getRGB() != shell_path.getRGB(x, y)){

                    position = new Vector2i(x, y);
                    contains = false;
                    for(List<Vector2i> c : contours)
                        if(c.contains(position)) {
                            contains = true;
                            break;
                        }

                    //Et si le pixel n'appartient pas déjà à un contour
                    if (!contains) {
                        depart = position;

                        contour_buffer = new ArrayList<>();

                        contour_buffer.add(position);
                        end = false;

                        //Tant que le parcours du contour n'est pas revenu à la position de départ
                        while (!end) {

                            neighbour = null;
                            adjacent_depart = false;

                            //Parcours des voisins du pixel courant
                            for (int i = 0; i < neighbours.length; ++i) {
                                neighbour = position.clone().add(neighbours[i]);

                                //Si le voisin est différent de noir et qu'il n'a pas déjà été visité
                                if(shell_path.getRGB(neighbour.m_X, neighbour.m_Y) != Color.black.getRGB()){
                                    if(!contour_buffer.contains(neighbour)) {
                                        contour_buffer.add(neighbour);
                                        position = neighbour;
                                        neighbour = null;
                                        break;
                                    }
                                    //Sinon on regarde si le pixel voisin est le pixel de départ du contour
                                    else if(neighbour == depart)
                                        adjacent_depart = true;
                                }
                            }

                            //Si aucun nouveau voisin n'a été trouvé et qu'un des voisin est le pixel de départ, alors on a fini de parcourir tout le contour
                            if(neighbour != null && adjacent_depart)
                                end = true;

                        }

                        contours.add(contour_buffer);
                        contour_buffer = null;
                    }
                }
            }
        }

        contours = smooth_contours(contours, tolerance_smoothing);

        return contours;
    }

    /**
     * Fonction qui applique un lissage avec la méthode de divide & conquer sur la liste de contours de la couche (NON TERMINE)
     * @param contours
     * @return
     */
    private List<List<Vector2i>> smooth_contours(List<List<Vector2i>> contours, float tolerance_smoothing){
        if(tolerance_smoothing <= 0)
            return contours;

        List<List<Vector2i>> result = new ArrayList<>();

        List<Vector2i> buffer;
        for(List<Vector2i> contour : contours){

        }


        return result;
    }

    /**
     * Fonction récursive de smoothing (NON TERMINE)
     * @param contour
     * @param begin
     * @param end
     * @return
     */
    private List<Vector2i> recursive_smooth(List<Vector2i> contour, int begin, int end, float tolerance_smoothing){
        int mid = (begin + end) / 2;

        Vector2i seg = contour.get(end).sub(contour.get(begin));
        float r = contour.get(mid).sub(contour.get(begin)).dot(seg) / (float)Math.pow(seg.length(),2);
        Vector2i proj_mid = contour.get(begin).add(seg.mul(r));
        float dist = proj_mid.sub(contour.get(mid)).length();

        if(Math.abs(dist) > tolerance_smoothing);

        return null;
    }

    /**
     *     Fonction qui générer le raster final zoomé de la couche pour export, avec la coque et le chemin de la buse lissé
     * @param pixels_per_unit
     * @param nozzle_diameter
     * @param zoom_ratio
     * @param tolerance_smoothing
     * @return
     */
    public BufferedImage final_raster(float pixels_per_unit, float nozzle_diameter, int zoom_ratio, float tolerance_smoothing){
        if(zoom_ratio == 0)
            return null;

        //Récupération des contours de l'image avec lissage
        List<List<Vector2i>> contours = get_contours(pixels_per_unit, nozzle_diameter, tolerance_smoothing);

        BufferedImage result = new BufferedImage(model.layers_dimensions[0] * zoom_ratio, model.layers_dimensions[1] * zoom_ratio, BufferedImage.TYPE_INT_RGB);

        //Tracé d'une image contenant la coque extérieure et le chemin de la buse en brut pour zoom (pixels rouges)
        BufferedImage brut_image = ImageUtils.fusion_img(draw_brut_raster(pixels_per_unit), shell_path_raster(pixels_per_unit, nozzle_diameter), null);

        //Tracé de l'image brute en zoom sur le raster final (pixel 1*1 image brute = pixel zoom_ratio*zoom_ratio image finale)
        Vector2i zoom_position;
        Graphics2D graphics = result.createGraphics();
        graphics.setColor(Color.red);
        for(int x = 0; x < model.layers_dimensions[0]; ++x){
            for (int y = 0; y < model.layers_dimensions[1]; ++y){
                if(brut_image.getRGB(x,y) != Color.black.getRGB()){
                    zoom_position = new Vector2i(x * zoom_ratio, y * zoom_ratio);
                    for(int i = 0; i < zoom_ratio; ++i)
                        graphics.drawLine(zoom_position.m_X, zoom_position.m_Y + i,
                                zoom_position.m_X + zoom_ratio -1, zoom_position.m_Y+i);

                }
            }
        }

        //Tracé des contours lissés sur l'image finale
        int center = zoom_ratio  /2;
        int x_begin, x_end, y_begin, y_end;
        graphics.setColor(Color.white);
        for(List<Vector2i> contour : contours){
            for(int i = 0; i < contour.size(); ++i){
                if(i == 0){
                    x_begin = contour.get(contour.size()-1).m_X * zoom_ratio + center;
                    y_begin = contour.get(contour.size()-1).m_Y * zoom_ratio + center;
                }
                else{
                    x_begin = contour.get(i-1).m_X * zoom_ratio + center;
                    y_begin = contour.get(i-1).m_Y * zoom_ratio + center;
                }
                x_end = contour.get(i).m_X * zoom_ratio + center;
                y_end = contour.get(i).m_Y * zoom_ratio + center;

                graphics.drawLine(x_begin, y_begin, x_end, y_end);
            }
        }
        //Release du graphics 2D
        graphics.dispose();

        //retourne l'image finale
        return result;
    }

    /**
     * Fonction qui génère le raster final zoomé avec de la couche pour export , avec chemin d'un pixel (ANCIENNE FONCTION)
     * @param neighbours
     * @param zoom_ratio
     * @param pixels_per_unit
     * @return
     */
    @Deprecated
    public BufferedImage final_raster_old(int[][] neighbours, int zoom_ratio, float pixels_per_unit)      {
        if(zoom_ratio == 0)
            return null;

        BufferedImage brut_raster = draw_brut_raster(pixels_per_unit);

        if(zoom_ratio == 1)
            return brut_raster;

        BufferedImage result = new BufferedImage( brut_raster.getWidth() * zoom_ratio, brut_raster.getHeight() * zoom_ratio, BufferedImage.TYPE_INT_RGB);
        Graphics2D writer = result.createGraphics();

        Color path_c = Color.white;

        int center = zoom_ratio/2;

        int new_x, new_y;
        boolean draw;

        //Parcours de tous les pixels de l'image
        for(int x = 0; x < brut_raster.getWidth(); ++x)
            for(int y = 0; y < brut_raster.getHeight(); ++y){
                if(Color.black.getRGB() != brut_raster.getRGB(x,y)){
                    new_x = x * zoom_ratio;
                    new_y = y * zoom_ratio;

                    writer.setColor(new Color(255,0,0));

                    //Draw du pixel de dimensions zoom_ratio dans le raster final
                    for(int i = 0; i < zoom_ratio; ++i)
                        writer.drawLine(new_x,new_y + i, new_x + zoom_ratio - 1, new_y + i);

                    //Parcours des voisins autour du pixel courant dans le raster brut (voisins connexe 8 ici)
                    for(int n = 0; n < neighbours.length; ++n){
                        int neigh_x = x + neighbours[n][0];
                        int neigh_y = y + neighbours[n][1];
                        try{
                            draw = brut_raster.getRGB(neigh_x, neigh_y) != Color.black.getRGB();
                            //Si le voisin est colorié
                            if(draw) {

                                //Regarde si le voisin est un voisin orthogonal ou non
                                draw = (n % 2) == 0;

                                //Si c'est un voisin diagonal, on regarde si les deux voisins orthogonaux adjacents à celui ci ne sont pas coloriés
                                if (!draw)
                                    draw = (brut_raster.getRGB(x + neighbours[(n - 1) % neighbours.length][0], y + neighbours[(n - 1) % neighbours.length][1]) == Color.black.getRGB()) &&
                                            (brut_raster.getRGB(x + neighbours[(n + 1) % neighbours.length][0], y + neighbours[(n + 1) % neighbours.length][1]) == Color.black.getRGB());
                            }

                            //Si les conditions sont validés, on trace un drawline entre le centre du pixel et du voisin courants
                            if(draw){
                                writer.setColor(path_c);
                                writer.drawLine(
                                        new_x + center,
                                        new_y + center,
                                        (neigh_x * zoom_ratio) + center,
                                        (neigh_y * zoom_ratio) + center
                                );
                            }
                        }catch(ArrayIndexOutOfBoundsException e){}
                    }
                }
            }
        writer.dispose();
        return result;
    }

    /**
     * Convertit tous les points de contour dans l'espace image avec changement de repere
     * @param pixels_per_unit
     * @return
     */
    public void discretise_layer(float pixels_per_unit){
        discret_pts = new ArrayList<>();

        for(Vector3f p : vectorial_pts)
            discret_pts.add(new Vector2i(
                    (int) ((p.m_X - model.origin[0]) * pixels_per_unit),
                    (int) ((p.m_Y - model.origin[1]) * pixels_per_unit)
            ));

    }
}
