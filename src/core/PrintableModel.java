package core;

import com.owens.oobjloader.builder.Build;
import com.owens.oobjloader.builder.Face;
import com.owens.oobjloader.parser.Parse;
import utils.ImageUtils;
import utils.Math.AABB;
import utils.Math.Vector3f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PrintableModel {

    /**
     * Classe statique dans laquelle sont stockés les différents paramètres utilisées pour la génération des couches, rasters, etc ...
     */
    public static class PrintableModelParameters{
        public static float layer_thickness = 0.2f; //Epaisseur d'une couche d'impression
        public static Vector3f padding_aabb = new Vector3f(10f,10f,0); //Padding ajouté autour du modèle 3d
        public static float pixels_per_unit = 1f / 0.05f; //Nombre de pixels par unité de float
        public static float nozzle_diameter = 0.4f; //diamètre en mm de la tête de buse d'impression
        public static int nb_depth_layers = 4; //Nombre de couches à afficher pour le depth peeling
        public static float smoothing_tolerance = 0;//(float)Math.sqrt(2); // seuil de tolérance pour le lissage des contours
        public static int zoom_ratio = 5; //facteur de zoom pour la génération du raster final des couche   s
    }

    List<Layer> layers; //Liste des couches du modèle
    AABB box; //Boite englobante du modèle 3d
    int[] layers_dimensions; //dimensions discrétisée des rasters bruts
    float[] origin; //position de l'origine utilisée pour la discrétisation des couches

    /**
     * Constructeur de la classe PrintableModel, prend en paramètre le chemin d'un fichier obj et construit les couches selon
     * les paramètres stockés dans la classe static PrintableModelParameters. Chaque couche est stockée sous la forme d'une instance de Layer,
     * contenant la liste des points en coordonnées vectorielles (3d float)
     * @param filename
     */
    public PrintableModel(String filename){
        Build model = null;
        layers = null;
        try{
            //Construction du modèle 3d à partir du fichier OBJ
            model = new Build();
            Parse obj = new Parse(model, new File(filename).toURI().toURL());

            //Création d'une boite englobante du modèle importé et ajout d'un padding selon le paramétrage
            box = new AABB(model.verticesG);
            box.add(PrintableModelParameters.padding_aabb);

            //Calcul des dimensions de l'image dans laquelle sera rasterisée chaque couche
            int width = (int) ((box.maxX - box.minX) * PrintableModelParameters.pixels_per_unit);
            int height = (int)((box.maxY - box.minY) * PrintableModelParameters.pixels_per_unit);
            layers_dimensions = new int[]{width, height};

            //stockage du point d'origine de la box
            origin = new float[]{box.minX,box.minY};

            //Construction d'une liste de couches avec un intervale égale à la valeur de layer_thickness (représente une épaisseur de couche)
            layers = new ArrayList<>();
            List<Vector3f> buffer;
            for(float z = box.minZ; z <= box.maxZ; z += PrintableModelParameters.layer_thickness){

                buffer = compute_single_layer(model, z);

                layers.add(new Layer(this, buffer));
            }

        }
        catch (java.io.IOException e)
        {
            System.out.println("IOException loading file "+filename+", e=" + e);
            e.printStackTrace();
        }
    }

    /**
     * Fonction qui génère les rasters brut du modèle
     * @return
     */
    public List<BufferedImage> draw_brut_rasters(){
        List<BufferedImage> rasters = new ArrayList<>(); //Liste dans laquelle seront stockées les couches

        BufferedImage buffer;

        for(int i = 0; i < layers.size(); ++i){
            buffer = layers.get(i).draw_brut_raster(PrintableModelParameters.pixels_per_unit);

            rasters.add(buffer);
        }

        return rasters;
    }

    /**
     * Fonction qui génère les couches remplies du modèle
     * @return
     */
    public List<BufferedImage> filled_rasters(){
        List<BufferedImage> rasters = new ArrayList<>(); //Liste dans laquelle seront stockées les couches

        BufferedImage buffer;

        for(int i = 0; i < layers.size(); ++i){
            buffer = layers.get(i).filled_raster(PrintableModelParameters.pixels_per_unit);
            rasters.add(buffer);
        }

        return rasters;
    }

    /**
     * Fonction qui va afficher pour chaque raster donné les n couches proches précédentes et suivantes, en interpollant une couleur arbitraire selon leur éloignement
     * @param filled Affichage du z depth avec uniquement les contours ou les couches remplies
     * @return
     */
    public List<BufferedImage> z_depth_rasters(boolean filled){

        List<BufferedImage> rasters = filled ? filled_rasters() : draw_brut_rasters();

        int coeff = (255 - 64) / (PrintableModelParameters.nb_depth_layers);

        List<BufferedImage> result = new ArrayList<>();

        for(int i = 0; i < rasters.size(); ++i){

            result.add(z_depth_single_raster(i, rasters, coeff));
        }

        return result;
    }

    /**
     * Fonction qui va afficher la coque extérieure et le chemin suivi par la buse pour chaque couche
     * @return une liste de raster, chacun représentant une couche où le chemin de la buse ainsi que la coque extérieure sont représentée
     */
    public List<BufferedImage> shell_path_rasters(){
        List<BufferedImage> result = new ArrayList<>();

        BufferedImage buffer;

        for(int i = 0; i < layers.size(); ++i){
            buffer = layers.get(i).shell_path_raster(PrintableModelParameters.pixels_per_unit, PrintableModelParameters.nozzle_diameter);
            result.add(buffer);
        }

        return result;
    }

    /**
     * Fonction qui va exporter les rasters finaux de toutes les couches de l'objet
     * @param folder_path
     */
    public void export_final_raster(String folder_path){
        int i = 0;
        for(Layer layer : layers){
            try{
                ImageIO.write(
                        layer.final_raster(PrintableModelParameters.pixels_per_unit, PrintableModelParameters.nozzle_diameter, PrintableModelParameters.zoom_ratio,
                                PrintableModelParameters.smoothing_tolerance),
                        "png",
                        new File(folder_path+"couche "+i+".png")
                );
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Fonction qui exporte raster final des couches avec chemin d'un pixel d'épaisseur (ANCIENNE FONCTION)
     * @param folder_path
     */
    @Deprecated
    public void export_raster_old(String folder_path){
        int[][] neighbours = new int[][]{new int[]{-1,0},new int[]{-1,-1},new int[]{0,-1},new int[]{1,-1},new int[]{1,0}, new int[]{1,1},new int[]{0,1},new int[]{-1,1}};

        BufferedImage buffer;
        for(    int i = 0; i < layers.size(); ++i){
            try{
                buffer = layers.get(i).final_raster_old(neighbours, PrintableModelParameters.zoom_ratio, PrintableModelParameters.pixels_per_unit);
                System.out.println("Export couche finale old "+i+" sur "+ layers.size());
                ImageIO.write(buffer,"png",new File(folder_path+"couche"+i+".png"));
                buffer = null;
            }
            catch(java.io.IOException e){
                System.out.println("IOException "+e);
                e.printStackTrace();
            }
        }
    }

    /**
     * Zdepth d'une couche avec les nbLayers adjacents dans le tableau, retourne le résultat obtenu
     * @param index
     * @param rasters
     * @param coeff_color
     * @return
     */
    private BufferedImage z_depth_single_raster(int index, List<BufferedImage> rasters, int coeff_color){
        BufferedImage under = new BufferedImage(layers_dimensions[0], layers_dimensions[1], BufferedImage.TYPE_INT_RGB);
        BufferedImage upper = new BufferedImage(layers_dimensions[0], layers_dimensions[1], BufferedImage.TYPE_INT_RGB);


        Color c;
        //Ajout de toutes les couches adjacentes à la couche d'index i avec interpolation de la couleur selon leur éloignement
        for(int i = 1; i <= PrintableModelParameters.nb_depth_layers; ++i){

            //Interpolation de la couleur selon l'éloigement de la couche en z
            c = new Color(255 - (i) * coeff_color,0,0);

            //Ajout de la couche inférieure index-i si indice non hors du tableau
            if(index - i >=0){
                under = ImageUtils.fusion_img( ImageUtils.fusion_img(new BufferedImage(layers_dimensions[0], layers_dimensions[1], BufferedImage.TYPE_INT_RGB),
                        rasters.get(index - i),c) , under, null);
            }

            //Ajout de la couche supérieure index+i si indice non hors du tableau
            if((index + i) < rasters.size()){
                upper = ImageUtils.fusion_img(upper, rasters.get(index + i), c);
            }
        }
        BufferedImage result = ImageUtils.fusion_img(under, rasters.get(index), new Color(255,0,0));
        result = ImageUtils.fusion_img(result, upper, null);
        return result;

    }

    /**
     * Fonction qui va discretiser toutes les couches en 2D selon le paramètre de résolution (nb de pixels par unité)
     */
    public void discretise_layers(){
        for(int i = 0; i < layers.size(); ++i)
            layers.get(i).discretise_layer(PrintableModelParameters.pixels_per_unit);
    }

    /**
     * Fonction qui calcule les points d'intersections entre un modele 3d et un plan à la hauteur z
     * @param model
     * @param z
     * @return
     */
    private List<Vector3f> compute_single_layer(Build model, float z){
        //Liste de vertices traçant le contour
        List<Vector3f> pts = new ArrayList<>();

        // Enumeration des faces (souvent des triangles, mais peuvent comporter plus de sommets dans certains cas
        List<Vector3f> vertices;

        for (Face face : model.faces)
        {
            //récupération des vertices de la face
            vertices = new ArrayList<>();
            vertices.add(new Vector3f(face.vertices.get(0).v.x, face.vertices.get(0).v.y, face.vertices.get(0).v.z));
            vertices.add(new Vector3f(face.vertices.get(1).v.x, face.vertices.get(1).v.y, face.vertices.get(1).v.z));
            vertices.add(new Vector3f(face.vertices.get(2).v.x, face.vertices.get(2).v.y, face.vertices.get(2).v.z));

            //Récupération de la hauteur min et min en z du triangle actuel pour savoir si le plan en z_pos le coupe ou non
            float max_z_face = Float.NEGATIVE_INFINITY;
            float min_z_face = Float.POSITIVE_INFINITY;
            for (int i = 0; i < 3; ++i) {
                if (vertices.get(i).m_Z > max_z_face) {
                    max_z_face = vertices.get(i).m_Z;
                }
                if (vertices.get(i).m_Z < min_z_face) {
                    min_z_face = vertices.get(i).m_Z;
                }
            }

            List<Vector3f> plan_cut_inters;
            //Si le plan coupe le triangle actuel, on calcule les points d'intersections des segments coupés
            if (z >= min_z_face && z <= max_z_face) {

                plan_cut_inters = new ArrayList<>();

                Vector3f inters;
                for (int i = 0; i < 3; ++i) {
                    if (i == 0)
                        inters = Vector3f.intersectZ(vertices.get(2), vertices.get(0), z);
                    else
                        inters = Vector3f.intersectZ(vertices.get(i - 1), vertices.get(i), z);

                    //Ajout du point trouvé aux intersection du triangle courant si non null et non déjà ajouté (cas où plan coupe un sommet du triangle => peut trouver deux inters)
                    if (inters != null && !plan_cut_inters.contains(inters))
                        plan_cut_inters.add(inters);
                }

                //Ajout du segment dans liste des point si deux inters trouvées
                if(plan_cut_inters.size() == 2)
                    pts.addAll(plan_cut_inters);

                    //Si trois inters trouvées, ajout des trois segments de la face (3 sommets + premier à nouveau pour compléter troisième segment)
                    //Cependant, cela signifie qu'un segment traverse la couche dans le contour et doit être supprimé de la liste
                else if (plan_cut_inters.size() == 3){

                    Vector3f deb, end;
                    boolean contains;

                    for(int i = 0; i < 3; ++i){
                        deb = plan_cut_inters.get(i);
                        end = plan_cut_inters.get(i < 2 ? i+1 : 0);
                        contains = false;

                        //Pour chaque segment du triangle
                        for(int j = 0; j < pts.size() - 1; j+=2){
                            //Si le segment est présent dans la liste, cela signifie que c'est un segment traversant et doit être donc supprimé
                            if((pts.get(j).equals(deb) && pts.get(j+1).equals(end))
                                    || (pts.get(j).equals(end) && pts.get(j+1).equals(deb))) {
                                pts.remove(j);
                                pts.remove(j+1);
                                contains = true;
                                break;
                            }
                        }
                        //Si il n'est pas contenu dans la liste, on l'ajoute (fait parti potentiellement du contour)
                        if(!contains){
                            pts.add(deb);
                            pts.add(end);
                        }
                    }
                }
            }
        }

        List<Vector3f> occur = new ArrayList<>();

        //Optimisation du nombre de points (suppression des instances identiques)
        for(int i = 0; i < pts.size(); ++i){
            if(!occur.contains(pts.get(i)))
                occur.add(pts.get(i));
            else{
                pts.set(i, occur.get(occur.indexOf(pts.get(i))));
            }
        }

        return pts;
    }
}
