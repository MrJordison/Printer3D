package fill_polygon;

public class Equation2i {
	public int a ;
	public int b ;
	public int c ;
	
	public Equation2i(Segment2i arete){
		
		//Calcul du delta y
		a = -(arete.v2.m_Y - arete.v.m_Y) ;
		
		//Calcul du delta x
		b = arete.v2.m_X - arete.v.m_X ;
		
		c = -((a * arete.v.m_X) + (b * arete.v.m_Y)) ;
	}
}

