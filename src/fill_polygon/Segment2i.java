package fill_polygon;

import utils.Math.Vector2i;

public class Segment2i implements Comparable<Segment2i>{
	public Vector2i v;
	public Vector2i v2;
	int delta_x;
	int delta_y;
	
	public Segment2i(){
		v = new Vector2i();
		v2 = new Vector2i();
		delta_x = delta_y = 0;
	}
	
	public Segment2i(Vector2i v, Vector2i v2){
		if(v.m_Y < v2.m_Y){
			this.v = v2 ;
			this.v2 = v ;
		}
		else{
			this.v = v ;
			this.v2 = v2 ;
		}

		delta_x = v.m_X - v2.m_X ;
		delta_y = v.m_Y - v2.m_Y ;
	}
	
	public int compareTo(Segment2i s){
		if(v.m_Y > s.v.m_Y){
			return -1 ;
		}
		else if (v.m_Y < s.v.m_Y){
			return 1;
		}
		else if(v.m_X < s.v.m_X){
			return -1;
		}
		else if(v.m_X > s.v.m_X){
			return 1;
		}
		return 0 ;
	}
	
	
	public int getYMax(){
		if(v.m_Y > v2.m_Y)
			return v.m_Y ;
		return v2.m_Y ;
	}
	
	public int getYMin(){
		if (v.m_Y < v2.m_Y)
			return v.m_Y ;
		return v2.m_Y ;
	}
	
	public int getXMax(){
		if(v.m_X > v2.m_X)
			return v.m_X ;
		return v2.m_X ;
	}
	
	public int getXMin(){
		if (v.m_X < v2.m_X)
			return v.m_X ;
		return v2.m_X ;
	}
	
	public String toString(){
		return v.toString()+" ; "+v2.toString();
	}


	public boolean equals(Object o){
		Segment2i s = (Segment2i) o;
		return v.equals(s.v) && v2.equals(s.v2);
	}
}
