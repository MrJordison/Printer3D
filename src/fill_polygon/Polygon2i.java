package fill_polygon;

import utils.Math.Vector2i;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Polygon2i {
	
	public List<Vector2i> sommets ;
	public List<Segment2i> aretes ;

	
	public Polygon2i(){
		this.sommets = new ArrayList<>() ;
		this.aretes = new ArrayList<>() ;
	}
	
	public Polygon2i(List<Vector2i> sommets, boolean stocked_as_segs){
	    if(!stocked_as_segs){
            this.sommets = sommets ;
            this.aretes = new ArrayList<>() ;
            for(int i = 0 ; i < sommets.size(); ++i){
                if(i == sommets.size() - 1){
                    aretes.add(new Segment2i(sommets.get(i),sommets.get(0)));

                }
                else{
                    aretes.add(new Segment2i(sommets.get(i),sommets.get(i+1)));

                }
            }
        }
        else{
	        this.sommets = new ArrayList<>();
	        this.aretes = new ArrayList<>();
	        Segment2i buffer;
	        for(int i = 0; i < sommets.size(); i+=2){
                buffer = new Segment2i(sommets.get(i), sommets.get(i+1));
                boolean b = aretes.contains(buffer);
                if(!b){
                    aretes.add(buffer);

                    if(!this.sommets.contains(sommets.get(i)))
                        this.sommets.add(sommets.get(i));
                    if(!this.sommets.contains(sommets.get(i+1)))
                        this.sommets.add(sommets.get(i+1));
                }
            }
        }

	}
	
	public void add(Vector2i v){
		sommets.add(v);
		if(sommets.size() > 1){
			if(sommets.size() > 2){
				aretes.remove(aretes.size()-1);
			}
			aretes.add(new Segment2i(sommets.get(sommets.size()-2),sommets.get(sommets.size()-1)));
			aretes.add(new Segment2i(sommets.get(sommets.size()-1),sommets.get(0)));
		}
	}

	public static BufferedImage fill_polygon(Polygon2i p, int[] dimensions){
		BufferedImage result = new BufferedImage(dimensions[0], dimensions[1], BufferedImage.TYPE_INT_RGB);
		Graphics writer = result.getGraphics();
		writer.setColor(Color.white);

		List<Integer> hauteurs = new ArrayList<>();
		for(Vector2i v : p.sommets)
			if(!hauteurs.contains(v.m_Y))
				hauteurs.add(v.m_Y);

		Collections.sort(hauteurs);

		int y_max, y_min;

		List<Integer> segs_zone, segs_prec_zone, intersections;
		segs_zone = new ArrayList<>();

		//Parcours de chaque zone
		for(int i = 0; i < hauteurs.size() - 1; ++i){

			//Récupération des limites de la zone y_min et y_max
			y_max = hauteurs.get(i + 1);
			y_min = hauteurs.get(i);

			segs_prec_zone = segs_zone;
			segs_zone = new ArrayList<>();

			//Ajout des segments de la zone précédente si leur y_max est supérieur à y_min actuel
			for(int ind_s : segs_prec_zone)
				if(p.aretes.get(ind_s).getYMax() > y_min)
					segs_zone.add(ind_s);

			//Ajout de nouveaux segments pour la zone en cours
			for(int j = 0; j < p.aretes.size(); ++j)
				if(p.aretes.get(j).getYMin() == y_min && !segs_prec_zone.contains(j))
					segs_zone.add(j);

			//Pour chaque scanline y entre y_min et y_max exclu (sauf dernière couche)
			for(int y = y_min; y < ((i == hauteurs.size() - 1) ? y_max + 1 : y_max); ++y){
				intersections = new ArrayList<>();

				//Calcul de chaque intersection si inexistante avec les segments
				for(int ind_seg : segs_zone){
					if(p.aretes.get(ind_seg).delta_y == 0 && p.aretes.get(ind_seg).getYMax() == y) {
						intersections.add(p.aretes.get(ind_seg).getXMin());
						intersections.add(p.aretes.get(ind_seg).getXMax());
					}
					else{
						Equation2i e = new Equation2i(p.aretes.get(ind_seg)) ;
						if(e.a != 0) {
							int x = (-((e.b * y) + e.c)) / e.a;
							intersections.add(x);
						}
					}

				}

				//Tri des intersections
				Collections.sort(intersections);

				//remplissage deux à deux entre les intersections
				for(int ind_x = 0; ind_x < intersections.size() - 1; ind_x+=2){
					writer.drawLine(intersections.get(ind_x), y,
							intersections.get(ind_x + 1), y);
				}

			}


		}
		writer.dispose();
		return result;
	}
}
