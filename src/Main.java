import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.util.List;


import core.PrintableModel;
import javax.imageio.ImageIO;

public class Main
{
	public static void main(String[] args){


        PrintableModel obj = new PrintableModel("resources/CuteOcto.obj");
        obj.discretise_layers();


        try{
            new File("./brut_rasters/").mkdir();
            new File("./peeling_rasters/").mkdir();
            new File("./old_final_rasters/").mkdir();
            new File("./fill_rasters/").mkdir();
            new File("./shell_rasters/").mkdir();
            new File("./final_rasters/").mkdir();



            //Export raster bruts
            List<BufferedImage> rasters = obj.draw_brut_rasters();
            for(int i = 0; i < rasters.size(); ++i) {
                System.out.println("Export couche simple : "+(i+1)+" sur "+rasters.size());
                ImageIO.write(rasters.get(i), "png", new File("./brut_rasters/couche" + i + ".png"));
            }


            //Export filled rasters
            /*List<BufferedImage> fill_rasters = obj.filled_rasters();
            for(int i = 0; i < fill_rasters.size(); ++i) {
                System.out.println("Export couche filled : "+(i+1)+" sur "+fill_rasters.size());
                ImageIO.write(fill_rasters.get(i), "png", new File("./fill_rasters/couche" + i + ".png"));
            }*/

            //Export peeling
            /*List<BufferedImage> peeling = obj.z_depth_rasters(true);
            for(int i = 0; i < peeling.size(); ++i){
                System.out.println("Export couche peeling : "+(i+1)+" sur "+peeling.size());
                ImageIO.write(peeling.get(i),"png",new File("./peeling_rasters/couche"+i+".png"));
            }*/

            //Export chemin buse intérieur
            /*List<BufferedImage> shell_rasters = obj.shell_path_rasters();
            for(int i = 0; i < shell_rasters.size(); ++i){
                System.out.println("Export couche shell : "+(i+1)+" sur "+shell_rasters.size());
                ImageIO.write(shell_rasters.get(i),"png",new File("./shell_rasters/couche"+i+".png"));
            }*/

            //Export raster finaux
            //obj.export_final_raster("./final_rasters/");

            //Old export raster finaux
            //obj.export_raster_old("./old_final_rasters/");


        }
        catch(IOException e){e.printStackTrace();}



    }
}
