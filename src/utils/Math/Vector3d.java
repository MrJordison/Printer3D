package utils.Math;

public class Vector3d
{
	public double m_X;
	public double m_Y;
	public double m_Z;

	public Vector3d(){
	    m_X = 0;
	    m_Y = 0;
	    m_Z = 0;
    }

    public Vector3d(double value){
	    m_X = m_Y = m_Z = value;
    }

	public Vector3d(double x, double y, double z)
	{
		this.m_X = x;
		this.m_Y = y;
		this.m_Z = z;
	}
	
	public Vector3d mul(double k)
	{
		return new Vector3d(k*m_X, k*m_Y, k*m_Z);
	}
	
	public Vector3d div(double k)
	{
		return new Vector3d(m_X/k, m_Y/k, m_Z/k);
	}
	
	public Vector3d add(Vector3d v2)
	{
		return new Vector3d(m_X + v2.m_X, m_Y + v2.m_Y, m_Z + v2.m_Z);
	}
	
	public Vector3d sub(Vector3d v2)
	{
		return new Vector3d(m_X - v2.m_X, m_Y - v2.m_Y, m_Z - v2.m_Z);
	}
	
	public double dot(Vector3d v2)
	{
		return m_X*v2.m_X + m_Y*v2.m_Y + m_Z*v2.m_Z;
	}
	
	public double length()
	{
		return Math.sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z);
	}
	
	public Vector3d norm()
	{
		double length = length();
		if (length != 0.0)
			return this.div(length); 
		else
			return this;
	}
	
	public Vector3d cross(Vector3d v2)
	{
		return new Vector3d(
				m_Y * v2.m_Z - m_Z * v2.m_Y,
				m_Z * v2.m_X - m_X * v2.m_Z,
				m_X * v2.m_Y - m_Y * v2.m_X);
	}

	public String toString(){
	    return "("+m_X+";"+m_Y+";"+m_Z+")";
    }

    public Vector3d clone(){
	    return new Vector3d(m_X,m_Y,m_Z);
    }
}

