package utils.Math;

public class Vector2i
{
	public int m_X;
	public int m_Y;

	public Vector2i(){
	    m_X = 0;
	    m_Y = 0;
    }

    public Vector2i(int value){
	    m_X = m_Y = value;
    }

	public Vector2i(int x, int y)
	{
		this.m_X = x;
		this.m_Y = y;
	}
	
	public Vector2i mul(float k)
	{
		return new Vector2i((int)(k*m_X), (int)(k*m_Y));
	}
	
	public Vector2i div(float k)
	{
		return new Vector2i((int)(m_X/k), (int)(m_Y/k));
	}
	
	public Vector2i add(Vector2i v2)
	{
		return new Vector2i(m_X + v2.m_X, m_Y + v2.m_Y);
	}
	
	public Vector2i sub(Vector2i v2)
	{
		return new Vector2i(m_X - v2.m_X, m_Y - v2.m_Y);
	}
	
	public float dot(Vector2i v2)
	{
		return m_X*v2.m_X + m_Y*v2.m_Y ;
	}
	
	public float length()
	{
		return (float)Math.sqrt(m_X*m_X + m_Y*m_Y);
	}
	
	public Vector2i norm()
	{
		float length = length();
		if (length != 0.0)
			return this.div(length); 
		else
			return this;
	}
	
	public int cross(Vector2i v2)
	{
		return m_X * v2.m_Y - v2.m_X * m_Y;
	}

	public String toString(){
	    return "("+m_X+";"+m_Y+")";
    }

    public Vector2i clone(){
	    return new Vector2i(m_X,m_Y);
    }

    public boolean equals(Object o){
        Vector2i v = (Vector2i) o;
        return (m_X == v.m_X) && (m_Y == v.m_Y);
    }
}

