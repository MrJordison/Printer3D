package utils.Math;

import com.owens.oobjloader.builder.FaceVertex;
import com.owens.oobjloader.builder.VertexGeometric;
import utils.Math.Vector3f;

import java.util.List;

public class AABB {

    public float minX, minY, minZ, maxX, maxY, maxZ;

    public AABB(List<VertexGeometric> vertices){

        minX = minY = minZ = Float.POSITIVE_INFINITY;
        maxX = maxY = maxZ = Float.NEGATIVE_INFINITY;

        for (VertexGeometric vertex : vertices){
            float x = vertex.x;
            float y = vertex.y;
            float z = vertex.z;

            if(minX > x)
                minX = x;
            else if(maxX < x)
                maxX = x;
            if(minY > y)
                minY = y;
            else if(maxY < y)
                maxY = y;
            if(minZ > z)
                minZ = z;
            else if(maxZ < z)
                maxZ = z;
        }
    }

    public void scale(float value){
        minX *= value;
        minY *= value;
        minZ *= value;
        maxX *= value;
        maxY *= value;
        maxZ *= value;
    }

    public void add(float value){
        minX += -value;
        minY += -value;
        minZ += -value;
        maxX += value;
        maxY += value;
        maxZ += value;
    }

    public void add(Vector3f v){
        minX -= v.m_X;
        minY -= v.m_Y;
        minZ -= v.m_Z;
        maxX += v.m_X;
        maxY += v.m_Y;
        maxZ += v.m_Z;
    }
}
