package utils.Math;

import java.util.Vector;

public class Vector3f
{
	public float m_X;
	public float m_Y;
	public float m_Z;

	public Vector3f(){
	    m_X = 0;
	    m_Y = 0;
	    m_Z = 0;
    }

    public Vector3f(float value){
	    m_X = m_Y = m_Z = value;
    }

	public Vector3f(float x, float y, float z)
	{
		this.m_X = x;
		this.m_Y = y;
		this.m_Z = z;
	}
	
	public Vector3f mul(float k)
	{
		return new Vector3f(k*m_X, k*m_Y, k*m_Z);
	}
	
	public Vector3f div(float k)
	{
		return new Vector3f(m_X/k, m_Y/k, m_Z/k);
	}
	
	public Vector3f add(Vector3f v2)
	{
		return new Vector3f(m_X + v2.m_X, m_Y + v2.m_Y, m_Z + v2.m_Z);
	}
	
	public Vector3f sub(Vector3f v2)
	{
		return new Vector3f(m_X - v2.m_X, m_Y - v2.m_Y, m_Z - v2.m_Z);
	}
	
	public float dot(Vector3f v2)
	{
		return m_X*v2.m_X + m_Y*v2.m_Y + m_Z*v2.m_Z;
	}
	
	public float length()
	{
		return (float)Math.sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z);
	}
	
	public Vector3f norm()
	{
		float length = length();
		if (length != 0.0)
			return this.div(length); 
		else
			return this;
	}
	
	public Vector3f cross(Vector3f v2)
	{
		return new Vector3f(
				m_Y * v2.m_Z - m_Z * v2.m_Y,
				m_Z * v2.m_X - m_X * v2.m_Z,
				m_X * v2.m_Y - m_Y * v2.m_X);
	}

	public String toString(){
	    return "("+m_X+";"+m_Y+";"+m_Z+")";
    }

    public Vector3f clone(){
	    return new Vector3f(m_X,m_Y,m_Z);
    }

    public boolean equals(Object o){
        Vector3f v = (Vector3f) o;
        boolean a = m_X == v.m_X;
        boolean b = m_Y == v.m_Y;
        boolean c = m_Z == v.m_Z;
    	return (a && b && c);
	}

    public static  int comparetoZ(Vector3f v, Vector3f v2){
        return v.m_Z > v2.m_Z ? 1 : v.m_Z == v2.m_Z ? 0 : -1;
    }

    /**
     * Méthode de calcul d'intersection entre un segment représenté par deux Vector3f et un plan en z
     * @param v
     * @param v2
     * @param z
     * @return
     */
    public static Vector3f intersectZ(Vector3f v, Vector3f v2, float z){
        //Si la première extrémité du segment est à la hauteur z, on la retourne
        if(v.m_Z == z)
            return v;

        int compare = comparetoZ(v,v2);
        Vector3f deb, end;
        //Si ils sont égaux, on retourne null, car coplanaires en z (cas où ils sont coplanaires à la hauteur d'intersection déjà géré avant)
        if(compare == 0)
            return null;
        else
            if(compare == -1){
                deb = v;
                end = v2;
            }
            else{
                deb = v2;
                end = v;
            }

        //Cas où il n'y a pas d'intersection avec le segment, retourne null
        if(!(deb.m_Z <= z && end.m_Z >= z))
            return null;

        //Calcul du ratio
        float dist_seg_z = Math.abs(end.m_Z - deb.m_Z);
        float dist_z = Math.abs(z - deb.m_Z);
        float ratio = dist_z/dist_seg_z;

        float new_x = deb.m_X + (end.m_X - deb.m_X) * ratio;
        float new_y = deb.m_Y + (end.m_Y - deb.m_Y) * ratio;

        return new Vector3f(new_x, new_y, z);
    }
}

