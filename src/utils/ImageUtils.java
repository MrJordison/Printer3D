package utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

public class ImageUtils {

    public static BufferedImage difference(BufferedImage base, BufferedImage to_sub){
        BufferedImage result = cloneBufferedImage(base);
        for(int x = 0; x < result.getWidth(); ++x)
            for(int y = 0; y < result.getHeight(); ++y)
                if(to_sub.getRGB(x, y) != Color.black.getRGB())
                    result.setRGB(x, y, Color.black.getRGB());
        return result;
    }

    public static BufferedImage erosion(BufferedImage raster){
        BufferedImage result = new BufferedImage(raster.getWidth(), raster.getHeight(), BufferedImage.TYPE_INT_RGB);

        boolean has_black_nb;

        int[][] neighbours = new int[][]{ {0,-1}, {-1,0}, {1, 0}, {0,1}};
        for(int x = 0; x < raster.getWidth(); ++x)
            for(int y = 0; y < raster.getHeight(); ++y){
                has_black_nb = false;
                for(int n = 0; n < neighbours.length; ++n)
                    try {
                        has_black_nb |= raster.getRGB(x + neighbours[n][0], y + neighbours[n][1]) == Color.black.getRGB();
                    }catch(ArrayIndexOutOfBoundsException e){}
                //Si le pixel courant n'a pas de voisin noir, on le dessine sur le résultat
                if(!has_black_nb)
                    result.setRGB(x, y, raster.getRGB(x, y));
            }

        return result;
    }

    /**
     * Fonction permettant de cloner une instance de bufferedImage
     * @param img
     * @return
     */
    public static BufferedImage cloneBufferedImage(BufferedImage img){
        ColorModel cm = img.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = img.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    /**
     * Fusionne deux images, la seconde est superposée sur la première, les deux doivent avoir la même taille
     * Possibilité de passer une couleur pour la couche overlap, sinon prend la couleur du pixel courant dans l'overlap
     * @param base
     * @param overlap
     * @return
     */
    public static BufferedImage fusion_img(BufferedImage base, BufferedImage overlap, Color color_overlap){

        int a,b,c,d;
        a = base.getWidth();
        b = base.getHeight();
        c = overlap.getWidth();
        d = overlap.getHeight();

        //Si tailles des rasters différentes, retourne null
        if(base.getWidth() != overlap.getWidth() || base.getHeight() != overlap.getHeight())
            return null;

        //Copie de l'image de base
        BufferedImage result = cloneBufferedImage(base);

        //Test si couleur passée en paramètre
        boolean colorSetUp = color_overlap != null;

        //Parcours de tous les pixel du raster overlap, si un pixel est différent de noir , on le dessine sur la couche
        //de base d'après la couleur passée en paramètre ou la couleur du pixel dans la couche overlap si paramètre null
        for(int x = 0; x < base.getWidth(); ++x)
            for(int y = 0; y < base.getHeight(); ++y){
                if(Color.black.getRGB() != overlap.getRGB(x,y)){
                    result.setRGB(x, y, colorSetUp ?  color_overlap.getRGB() : overlap.getRGB(x,y));
                }
            }
        return result;
    }
}
